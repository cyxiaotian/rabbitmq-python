
import pika
import time


# 用于进行 authentication credentials (encapsulate all authentication information for the 'ConnectionParameters' class)
credentials = pika.PlainCredentials('admin', 'admin', True)

# step1 创建连接
connection = pika.BlockingConnection(pika.ConnectionParameters(
    '192.168.1.198',
    5672,
    '/',
    credentials=credentials
))
# connection = pika.BlockingConnection(pika.ConnectionParameters(
#     '192.168.1.198',
#     5672,
#     '/',
#     credentials=credentials
# ))

# step2 声明一个管道（管道内用于进行消息发送）
channel = connection.channel()

# step3 声明queue消息队列
channel.queue_declare(
    queue='first_queue'         # the queue's declare name
)

send_msg = "this is the first message, send by rabbitmq!"
# step4
count = 0
while count < 200:
    channel.basic_publish(
        exchange='',
        routing_key='first_queue',
        body=send_msg + str(count).zfill(2)
    )
    print("we have send the message success!")
    count += 1
    time.sleep(1)

connection.close()
