
import pika
import time

# step1 创建连接
connection = pika.BlockingConnection(pika.ConnectionParameters('127.0.0.1'))

# step2 声明通道，避免read.py先启动
channel = connection.channel()

# step3 声明消息队列
channel.queue_declare(
    queue='first_queue'
)

# step4
def recv_callback(ch, method, properties, body):    # step4.1 定义回掉函数
    print("Received %r" % body)
    time.sleep(4)
    # 手动应答
    ch.basic_ack(delivery_tag=method.delivery_tag)


# step 6 设置为公平分发消息
channel.basic_qos(prefetch_count=1)

channel.basic_consume(
    queue='first_queue',
    on_message_callback=recv_callback,
    auto_ack=False
)

print("Waiting for message.")
# step5 启动消费监听
channel.start_consuming()


