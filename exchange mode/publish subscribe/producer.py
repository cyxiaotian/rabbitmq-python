#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author: caoy
# @Time:  14:21

import pika
import time

# step1 创建连接
credentials = pika.PlainCredentials('admin', 'admin', True)
connection = pika.BlockingConnection(pika.ConnectionParameters(
    'localhost',
    5672,
    '/',
    credentials=credentials
))
# step2 声明通道
channel = connection.channel()
# step3 声明交换机
channel.exchange_declare(
    'logs_fanout',
    'fanout'
)
# step4 创建生产者
count = 1
msg = "This message from producer."
while count < 10:
    channel.basic_publish(
        'logs_fanout',
        routing_key='',
        body=msg+str(count).zfill(2)
    )
    print("We have send the message success!")
    count += 1
    time.sleep(1)

# step5 关闭连接
connection.close()
