#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author: caoy
# @Time:  14:52

import pika

# step1 创建连接
credentials = pika.PlainCredentials('admin', 'admin', True)
connection = pika.BlockingConnection(pika.ConnectionParameters(
    'localhost',
    5672,
    '/',
    credentials=credentials
))
# step2 声明通道
channel = connection.channel()
# step3 声明交换机
channel.exchange_declare(
    'logs_direct',
    'direct'            # 交换机模式类型 direct为关键字模式
)

# step4 绑定队列与交换机
res = channel.queue_declare(queue='', exclusive=True)
queue_name = res.method.queue

channel.queue_bind(
    queue=queue_name,
    exchange='logs_direct',
    routing_key='error'         # 指定关键字
)

# channel.queue_bind(
#     queue=queue_name,
#     exchange='logs_direct',
#     routing_key='info'
# )


def callback(ch, method, properties, body):
    print("Received: %r" % body)

    ch.basic_ack(delivery_tag=method.delivery_tag)


# step5 消费消息
channel.basic_consume(
    queue=queue_name,
    auto_ack=False,
    on_message_callback=callback
)
print("current message queue name: %r" % queue_name)
print("Waiting for message.")

# step6 启动消费者
channel.start_consuming()


