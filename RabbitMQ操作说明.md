
## RabbitMQ Message Queue
RabbitMQ遵循AMQP(Advanced Message Queuing Protocol)协议。它是一种应用层协议的一个开放标准，为面向消息的中间件设计，基于此协议的客户端与消息中间件可传递消息，它不受产品、开发语言等条件的限制。能够跨进程、异步的通信机制，用于进行上下游传递消息。

### RabbitMQ 的作用
MQ主要功能是用于进行应用解耦、流量削峰、异步操作、数据分发、日志收集、错峰流控等等。

### RabbitMQ management 连接情况
默认情况下，rabbitmq management(网页)的guest账号只能支持采用`localhost:15672`的方式进行访问。不支持进行远程连接。 

### 1.本地连接与远程连接的区别
本例连接情况下，`ConnectionParameters`参数中，`host`, `port`如果不进行填写，参数采用默认值。默认值为`guest`。
采用远程连接时，需要对`ConnectionParameters`参数进行罗列，否则将产生如常见异常１描述的问题。
    
    # 示例代码
    # 用于进行 authentication credentials (encapsulate all authentication information for the 'ConnectionParameters' class)
    credentials = pika.PlainCredentials('admin', 'admin', True)
    
    # step1 创建连接
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        '192.168.1.198',    # ip
        5672,   # port
        '/',    # virtual host
        credentials=credentials
    ))


### 使用示例

#### 主要示例包括：
* 1 简单模式
* 2 参数配置情况
* 3 交换机模式
    - 发布订阅模式(**fanout**)
    - 关键字模式(**direct**)
    - 通配符模式(**topic**)
    
 参数配置情况同样可以适用于交换机模式。
 
 
 
#### 1 简单模式

简单模式用于模拟生产者与消费之的简单的队列操作。多个消费者之间采用轮询的方式接收生产者产生的消息数据。

##### 1.1 PUBLISH

    import pika
    import time

    # step1 创建连接
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        'localhost'
    ))

    # step2 声明一个管道（管道内用于进行消息发送）
    channel = connection.channel()

    # step3 声明queue消息队列
    channel.queue_declare(
        queue='first_queue'         # the queue's declare name
    )

    send_msg = "this is the first message, send by rabbitmq!"
    # step4
    count = 0
    while count < 5:
        channel.basic_publish(
            exchange='',
            routing_key='first_queue',
            body=send_msg + str(count).zfill(2)
        )
        print("we have send the message success!")
        count += 1
        time.sleep(1)

    connection.close()

##### 1.2 CONSUME

    import pika

    # step1 创建连接
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

    # step2 声明通道，避免read.py先启动
    channel = connection.channel()

    # step3 声明消息队列
    channel.queue_declare(
        queue='first_queue'
    )

    # step4
    def recv_callback(ch, method, properties, body):    # step4.1 定义回掉函数
        print("Received %r" % body)
        # 手动应答
        # ch.basic_ack(delivery_tag=method.delivery_tag)


    channel.basic_consume(
        queue='first_queue',
        on_message_callback=recv_callback,
        auto_ack=True   # True则为自动应答

    )

    print("Waiting for message.")
    # step5 启动消费监听
    channel.start_consuming()

> 通过设置`basic_consume`函数的`auto_ack`参数为`false`，以及在`callback`函数中末尾添加`ch.basic_ack(delivery_tag=method.delivery_tag)`语句，则可以实现手动处理应答信息。即消息不被回调函数处理完毕，不会在消息队列里清除消息。 

##### 1.3 公平分发

    import pika
    import time
    
    # step1 创建连接
    connection = pika.BlockingConnection(pika.ConnectionParameters('127.0.0.1'))
    
    # step2 声明通道，避免read.py先启动
    channel = connection.channel()
    
    # step3 声明消息队列
    channel.queue_declare(
        queue='first_queue'
    )
    
    # step4
    def recv_callback(ch, method, properties, body):    # step4.1 定义回掉函数
        print("Received %r" % body)
        time.sleep(4)
        # 手动应答
        ch.basic_ack(delivery_tag=method.delivery_tag)
    
    
    # step 6 设置为公平分发消息
    channel.basic_qos(prefetch_count=1)
    
    channel.basic_consume(
        queue='first_queue',
        on_message_callback=recv_callback,
        auto_ack=False
    )
    
    print("Waiting for message.")
    # step5 启动消费监听
    channel.start_consuming()


#### 2 参数配置

参数配置主要是对RabbitMQ参数变化时能够产生的影响的情况分析。涉及，公平分发（轮询）、自动应答（手动应答）、消息持久化等。

由于参数配置情况示例主题上并不与主体代码产生本质区别，主要在于对具体参数进行变化，故而公平分发以及手动应答可以参考简单模式示例进行修改。

> 示例：消息持久化

由于rabbitmq是借助于第三方软件进行的消息通信机制，因此会出现rabbitmq崩溃的情况，如果此时消息队列中存在未消费的消息，则需要考虑是否需要持久化以保证数据的安全。

##### 2.1 PUBLISH

    #!/usr/bin/env python3
    # -*- encoding:utf-8 -*-
    # Author: caoy
    # @Time:  10:40
    
    import pika
    import time
    
    # step1 创建连接
    credentials = pika.PlainCredentials('admin', 'admin', True)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        '192.168.1.198',            # host
        5672,                       # port
        '/',                        # virtual host  
        credentials=credentials
    ))
    
    # step2 声明通道
    channel = connection.channel()
    
    # step3 声明一个持久化的队列
    channel.queue_declare(
        'second_queue',
        durable=True
    )
    
    # step4 发送消息
    count = 1
    msg = 'The sending message, from second queue!'
    while count < 10:
        channel.basic_publish(
            '',     # exchange
            routing_key='second_queue',     # the binding queue's name
            body=msg+str(count).zfill(2),
            properties=pika.BasicProperties(
                delivery_mode=2     # 消息持久化
            )
        ) 
        print('Send the msg success %r times!' % count)
        count += 1
        time.sleep(2)
    
    # step5 关闭连接
    connection.close()

##### 2.2 CONSUME
采用手动应答方式。

    #!/usr/bin/env python3
    # -*- encoding:utf-8 -*-
    # Author: caoy
    # @Time:  10:40
    
    import pika
    
    # step1 创建连接
    credentials = pika.PlainCredentials('admin', 'admin', True)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        'localhost',
        5672,
        '/',
        credentials=credentials
    ))
    
    # step2 声明通道
    channel = connection.channel()
    
    # step3 声明一个持久化队列
    channel.queue_declare(
        'second_queue',
        durable=True
    )
    
    
    def callback(ch, method, properties, body):
        print("Received: %r" % body)
    
        ch.basic_ack(method.delivery_tag)
    
    
    # step4 创建消费
    channel.basic_consume(
        'second_queue',
        auto_ack=False,
        on_message_callback=callback
    )
    print("Waiting for message.")
    
    # step5 启动消费者
    channel.start_consuming()

#### 3 交换机模式
创建交换机模式时，由生产者创建交换机，消费者创建消息队列并绑定对应的交换机。生产者产生消息并发送给对应的交换机，由交换机分发给对应绑定的队列。

交换机模式下，应答模式应采用手动应答。
##### 3.1 发布订阅模式 **fanout**
> 示例: 发布订阅模式，类似于广播，属于实时发送。用于向多个client同步发送相同的消息。
##### 3.1.1 PUBLISH
    #!/usr/bin/env python3
    # -*- encoding:utf-8 -*-
    # Author: caoy
    # @Time:  14:21
    
    import pika
    import time
    
    # step1 创建连接
    credentials = pika.PlainCredentials('admin', 'admin', True)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        'localhost',
        5672,
        '/',
        credentials=credentials
    ))
    # step2 声明通道
    channel = connection.channel()
    # step3 声明交换机
    channel.exchange_declare(
        'logs_fanout',
        'fanout'
    )
    # step4 创建生产者
    count = 1
    msg = "This message from producer."
    while count < 10:
        channel.basic_publish(
            'logs_fanout',
            routing_key='',
            body=msg+str(count).zfill(2)
        )
        print("We have send the message success!")
        count += 1
        time.sleep(1)
        
    # step5 关闭连接
    connection.close()

##### 3.1.2 CONSUME
    #!/usr/bin/env python3
    # -*- encoding:utf-8 -*-
    # Author: caoy
    # @Time:  14:52
    
    import pika
    
    # step1 创建连接
    credentials = pika.PlainCredentials('admin', 'admin', True)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        'localhost',
        5672,
        '/',
        credentials=credentials
    ))
    # step2 声明通道
    channel = connection.channel()
    # step3 声明交换机
    channel.exchange_declare(
        'logs_fanout',
        'fanout'
    )
    
    # step4 绑定队列与交换机
    res = channel.queue_declare(queue='', exclusive=True)
    queue_name = res.method.queue
    
    channel.queue_bind(
        queue=queue_name,
        exchange='logs_fanout'
    )
    
    
    def callback(ch, method, properties, body):
        print("Received: %r" % body)
    
        ch.basic_ack(delivery_tag=method.delivery_tag)
    
    
    # step5 消费消息
    channel.basic_consume(
        queue=queue_name,
        auto_ack=False,
        on_message_callback=callback
    )
    print("current message queue name: %r" % queue_name)
    print("Waiting for message.")
    
    # step6 启动消费者
    channel.start_consuming()
    



##### 3.2 关键字模式 **direct**
关键字模式是对发布订阅模式的一种增强，在发布订阅模式的基础上增加了对消息关键字的指定routing_key关键字。
> 示例:通过指定routing_key关键字，进行生产消息时对关键字的指定。

##### 3.2.1 PUBLISH
    #!/usr/bin/env python3
    # -*- encoding:utf-8 -*-
    # Author: caoy
    # @Time:  14:21
        
    import pika
    import time
    
    # step1 创建连接
    credentials = pika.PlainCredentials('admin', 'admin', True)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        'localhost',
        5672,
        '/',
        credentials=credentials
    ))
    # step2 声明通道
    channel = connection.channel()
    # step3 声明交换机
    channel.exchange_declare(
        'logs_direct',
        'direct'
    )
    # step4 创建生产者
    count = 1
    msg = "This message from producer-error."
    while count < 2:
        channel.basic_publish(
            'logs_direct',
            routing_key='error',        # 指定关键字
            body=msg+str(count).zfill(2)
        )
        print("We have send the message success!")
        count += 1
        time.sleep(1)
    
    # step5 关闭连接
    connection.close()

##### 3.2.2 CONSUME

    #!/usr/bin/env python3
    # -*- encoding:utf-8 -*-
    # Author: caoy
    # @Time:  14:52
    
    import pika
    
    # step1 创建连接
    credentials = pika.PlainCredentials('admin', 'admin', True)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        'localhost',
        5672,
        '/',
        credentials=credentials
    ))
    # step2 声明通道
    channel = connection.channel()
    # step3 声明交换机
    channel.exchange_declare(
        'logs_direct',
        'direct'            # 交换机模式类型 direct为关键字模式
    )
    
    # step4 绑定队列与交换机
    res = channel.queue_declare(queue='', exclusive=True)
    queue_name = res.method.queue
    
    channel.queue_bind(
        queue=queue_name,
        exchange='logs_direct',
        routing_key='error'         # 指定关键字
    )
    
    # channel.queue_bind(
    #     queue=queue_name,
    #     exchange='logs_direct',
    #     routing_key='info'         # 指定关键字
    # )
    
    
    def callback(ch, method, properties, body):
        print("Received: %r" % body)
    
        ch.basic_ack(delivery_tag=method.delivery_tag)
    
    
    # step5 消费消息
    channel.basic_consume(
        queue=queue_name,
        auto_ack=False,
        on_message_callback=callback
    )
    print("current message queue name: %r" % queue_name)
    print("Waiting for message.")
    
    # step6 启动消费者
    channel.start_consuming()
    


##### 3.3 通配符模式 **topic**
通配符模式是在关键字模式基础上增加了模糊匹配的功能。
> 示例:通配符只接受两种: `#`, `*`。
##### 3.3.1 PUBLISH
    
    #!/usr/bin/env python3
    # -*- encoding:utf-8 -*-
    # Author: caoy
    # @Time:  14:21
    
    import pika
    import time
    
    # step1 创建连接
    credentials = pika.PlainCredentials('admin', 'admin', True)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        'localhost',
        5672,
        '/',
        credentials=credentials
    ))
    # step2 声明通道
    channel = connection.channel()
    # step3 声明交换机
    channel.exchange_declare(
        'logs_topic',
        'topic'
    )
    # step4 创建生产者
    count = 1
    msg = "This message from producer-china.shanghai."
    while count < 2:
        channel.basic_publish(
            'logs_topic',
            routing_key='china.shanghai',        # 指定关键字
            body=msg+str(count).zfill(2)
        )
        print("We have send the message success!")
        count += 1
        time.sleep(1)
    
    # step5 关闭连接
    connection.close()

##### 3.3.2 CONSUME

    #!/usr/bin/env python3
    # -*- encoding:utf-8 -*-
    # Author: caoy
    # @Time:  14:52
    
    import pika
    
    # step1 创建连接
    credentials = pika.PlainCredentials('admin', 'admin', True)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        'localhost',
        5672,
        '/',
        credentials=credentials
    ))
    # step2 声明通道
    channel = connection.channel()
    # step3 声明交换机
    channel.exchange_declare(
        'logs_topic',
        'topic'            # 交换机模式类型 direct为关键字模式
    )
    
    # step4 绑定队列与交换机
    res = channel.queue_declare(queue='', exclusive=True)
    queue_name = res.method.queue
    
    # channel.queue_bind(
    #     queue=queue_name,
    #     exchange='logs_topic',
    #     routing_key='china.#'         # 指定关键字
    # )
    
    channel.queue_bind(
        queue=queue_name,
        exchange='logs_topic',
        routing_key='#.shanghai'
    )
    
    
    def callback(ch, method, properties, body):
        print("Received: %r" % body)
    
        ch.basic_ack(delivery_tag=method.delivery_tag)
    
    
    # step5 消费消息
    channel.basic_consume(
        queue=queue_name,
        auto_ack=False,
        on_message_callback=callback
    )
    print("current message queue name: %r" % queue_name)
    print("Waiting for message.")
    
    # step6 启动消费者
    channel.start_consuming()
    
    


### 常见异常
#### 1.关于`ConnectionParameters`参数异常
>异常描述：pika.exceptions.ProbableAuthenticationError: ConnectionClosedByBroker: (403) 'ACCESS_REFUSED - Login was refused using authentication mechanism PLAIN. For details see the broker logfile.'

<!-- #解决方式：参见[1. 本地连接与远程连接的区别](###1.本地连接与远程连接的区别)描述。-->

#### 2.关于`auto_ack`参数设置异常
>异常描述：pika.exceptions.ChannelClosedByBroker: (406, 'PRECONDITION_FAILED - unknown delivery tag 1')