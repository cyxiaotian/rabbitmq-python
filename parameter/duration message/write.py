#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author: caoy
# @Time:  10:40

import pika
import time

# step1 创建连接
credentials = pika.PlainCredentials('admin', 'admin', True)
connection = pika.BlockingConnection(pika.ConnectionParameters(
    '192.168.1.198',            # host
    5672,                       # port
    '/',                        # virtual host
    credentials=credentials
))

# step2 声明通道
channel = connection.channel()

# step3 声明一个持久化的队列
channel.queue_declare(
    'second_queue',
    durable=True
)

# step4 发送消息
count = 1
msg = 'The sending message, from second queue!'
while count < 10:
    channel.basic_publish(
        '',     # exchange
        routing_key='second_queue',     # the binding queue's name
        body=msg+str(count).zfill(2),
        properties=pika.BasicProperties(
            delivery_mode=2     # 消息持久化
        )
    )

    print('Send the msg success %r times!' % count)
    count += 1
    time.sleep(2)

# step5 关闭连接
connection.close()

