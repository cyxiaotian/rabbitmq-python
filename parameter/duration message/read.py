#!/usr/bin/env python3
# -*- encoding:utf-8 -*-
# Author: caoy
# @Time:  10:40

import pika

# step1 创建连接
credentials = pika.PlainCredentials('admin', 'admin', True)
connection = pika.BlockingConnection(pika.ConnectionParameters(
    'localhost',
    5672,
    '/',
    credentials=credentials
))

# step2 声明通道
channel = connection.channel()

# step3 声明一个持久化队列
channel.queue_declare(
    'second_queue',
    durable=True
)


def callback(ch, method, properties, body):
    print("Received: %r" % body)

    ch.basic_ack(method.delivery_tag)


# step4 创建消费
channel.basic_consume(
    'second_queue',
    auto_ack=False,
    on_message_callback=callback
)
print("Waiting for message.")

# step5 启动消费者
channel.start_consuming()
